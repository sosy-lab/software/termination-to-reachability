# Termination to reachability

This project implements an idea from liveness to safety transformation by Armin Biere:

https://www.sciencedirect.com/science/article/pii/S1571066104804109

However, instead of instrumenting Kripke structure with memory and flag for condition satisfiability, we instrument the program (or CFA) instead.
